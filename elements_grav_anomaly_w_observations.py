import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib import figure


def npa_dg_elements_w_obs(drho1=100, drho2=100, drho3=100, x1=5, z1=10, x2=50, z2=30, x3=50, z3=30):
    G=6.67e-11  # Metric system is MKS -- distances in meters
    x_obs=[10,20,25,30,35,40,45,50,70,90]
    dg_obs=[0.2e-7, 0.5e-7, 1e-7, 3e-7, 3e-7, 1e-7, -1.2e-7, -0.2e-7, 0.4e-7, -0.2e-7]
    dg_err=1.0e-7

    x_all = []
    dg_all = []
    dx=1
    x=0
    i=0
    while i < 100:
        x+=1
        x_all.append(x)
        dg1 = ( drho1 * (z1)/( ((x-x1)**2 + z1**2)**(1.5) ) )
        dg2 = ( drho2 * (z2)/( ((x-x2)**2 + z2**2)**(1.5) ) )
        dg3 = ( drho3 * (z3)/( ((x-x3)**2 + z3**2)**(1.5) ) )
        dg = G* (dg1+ dg2 + dg3)
        dg_all.append(dg)
        i+=1

    depth1=-1.0*z1
    depth2=-1.0*z2
    depth3=-1.0*z3
    if drho1<0:
        color1="red"
    if drho1>=0:
        color1="blue"
    if drho2<0:
        color2="red"
    if drho2>=0:
        color2="blue"
    if drho3<0:
        color3="red"
    if drho3>=0:
        color3="blue"
        
    fig, (ax1,ax2) = plt.subplots(2,figsize=(15,6))
    ax1.plot(x_all,dg_all)
    ax1.errorbar(x_obs, dg_obs, yerr=dg_err, fmt='o')
    ax1.grid()
    ax1.set_ylabel('Gravity anomaly (m/s2)')
    ax1.set_xlim(0,100)
    ax1.set_ylim(-5e-7,5e-7)
    rect1 = patches.Rectangle((x1, depth1), 1, 1, facecolor=color1)
    ax2.add_patch( rect1 )
    rect2 = patches.Rectangle((x2, depth2), 1, 1, facecolor=color2)
    ax2.add_patch( rect2 )
    rect3 = patches.Rectangle((x3, depth3), 1, 1, facecolor=color3)
    ax2.add_patch( rect3 )
    ax2.grid()
    ax2.set_ylabel('Depth (m)')
    ax2.set_xlabel('x (m)')
    ax2.set_xlim(0,100)
    ax2.set_ylim(-20,0)
    plt.figure()
    
    return



