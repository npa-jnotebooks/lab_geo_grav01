import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import figure

def npa_dg_sphere(drho=100, R=5, z=10, xc=50):
    G=6.67e-11  # Metric system is MKS -- distances in meters
    cost=(4.0/3.0)*3.1415*G*(R**3)*drho
    x_all = []
    dg_all = []
    dx=1
    x=0
    i=0
    while i < 100:
        x+=1
        x_all.append(x)
        dg= ( cost * (z)/( ((x-xc)**2 + z**2)**(1.5) ) )
        dg_all.append(dg)
        i+=1
        
    if drho<0:
        color_sphere="red"
    else:
        color_sphere="blue"
        
    depth=-1.0*z
    fig, (ax1,ax2) = plt.subplots(2,figsize=(15,6))
    ax1.plot(x_all,dg_all)
    ax1.grid()
    ax1.set_ylabel('Gravity anomaly (m/s2)')
    ax1.set_xlim(0,100)
    ax1.set_ylim(-5e-7,5e-7)
    Drawing_colored_circle = plt.Circle(( xc , depth ), R, color=color_sphere ) 
    ax2.add_artist( Drawing_colored_circle )
    ax2.grid()
    ax2.set_ylabel('Depth (m)')
    ax2.set_xlabel('x (m)')
    ax2.set_xlim(0,100)
    ax2.set_ylim(-20,0)
    plt.figure()
    
    return



